import React, { useReducer, useEffect } from 'react';
import { withTheme } from '../../Context';
import Confirmation from './Confirmation';
import styled from 'styled-components/native';
import API, { API_KEY } from '../../helpers/api';
import { reducer, initialState } from './reducer';
import { Container, Button, Icon, Error } from '../../components';

const Hamburger = styled.TouchableOpacity`
  margin-left: 28px;
`;
const BtnText = styled.Text`
  color: #fff;
  margin-right: 10px;
`;
const StartInventory = ({ user, navigation }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const getInventoryRun = async () => {
    const { scanner_id, scanner_code } = user.scanner;
    try {
      const { data } = await API.get('/GetInventoryRun', {
        params: {
          api_key: API_KEY,
          scanner_id,
          scanner_code,
        },
      });
      if (data.inventory_run) {
        dispatch({ type: 'SET_UPDATES', payload: { error: null } });
        navigation.navigate('UpdateInventory');
      } else {
        dispatch({ type: 'SET_UPDATES', payload: { error: data.error_message } });
      }
    } catch ({ response }) {
      dispatch({ type: 'SET_UPDATES', payload: { error: 'Something wrong...' } });
    }
  };

  useEffect(() => {
    getInventoryRun();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container>
      <Button
        label="START INVENTORY"
        onPress={() => dispatch({ type: 'SET_UPDATES', payload: { visible: true } })}
      />
      {state.error && <Error>{state.error}</Error>}
      <Confirmation user={user} {...state} dispatch={dispatch} navigation={navigation} />
    </Container>
  );
};

StartInventory.navigationOptions = ({ navigation }) => ({
  headerLeft: (
    <Hamburger onPress={() => navigation.openDrawer()}>
      <Icon name="sidelist" color="#000" />
    </Hamburger>
  ),
});

export default withTheme(StartInventory);
