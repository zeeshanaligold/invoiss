export const initialState = {
  error: null,
  start: false,
  loader: false,
  visible: false,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_UPDATES':
      return { ...state, ...action.payload };
    default:
      throw new Error('Unexpected action');
  }
};
