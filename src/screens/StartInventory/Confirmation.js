import React from 'react';
import moment from 'moment';
import { Button } from '../../components';
import styled from 'styled-components/native';
import API, { API_KEY } from '../../helpers/api';
import { ActivityIndicator } from 'react-native';
import Modal, { ModalContent } from 'react-native-modals';

const StyledModal = styled(Modal)`
  padding: 0 20px;
`;
const Flex = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;
const BtnText = styled.Text`
  color: #fff;
  margin-right: 10px;
`;
const Text = styled.Text`
  margin: ${({ margin }) => margin && margin};
`;
const Confirmation = ({ user, loader, visible, dispatch, navigation }) => {
  const handleClickStart = async () => {
    const { scanner_id, scanner_code } = user.scanner;
    dispatch({ type: 'SET_UPDATES', payload: { loader: true } });
    try {
      const { data } = await API.get('/StartInventoryRun', {
        params: {
          api_key: API_KEY,
          scanner_id,
          scanner_code,
        },
      });
      if (data.inventory_run) {
        dispatch({ type: 'SET_UPDATES', payload: { visible: false, loader: false } });
        navigation.navigate('UpdateInventory');
      } else {
        dispatch({
          type: 'SET_UPDATES',
          payload: { error: data.error_message, loader: false, visible: false },
        });
      }
    } catch ({ response }) {
      dispatch({
        type: 'SET_UPDATES',
        payload: { error: 'Something wrong...', loader: false, visible: false },
      });
    }
  };

  return (
    <StyledModal visible={visible}>
      <ModalContent>
        <Text margin="0">
          YOU ARE ABOUT TO START INVENTORY. THE DATE AND TIME WILL BE NOTED.{' '}
          {moment().format('MM/DD/YYYY  HH:mmA')}
        </Text>
        <Text margin="10px 0 30px">
          ANY SCANNED ITEM WILL BE TRANSFERRED TO YOUR INVENTORY PAGE IN REALTIME.
        </Text>
        <Flex>
          <Button
            label="Cancel"
            width="100px"
            onPress={() =>
              dispatch({
                type: 'SET_UPDATES',
                payload: { visible: false },
              })
            }
          />
          <Button width="100px" onPress={handleClickStart}>
            <BtnText>Start</BtnText>
            {loader && <ActivityIndicator size="small" color="#fff" animating={loader} />}
          </Button>
        </Flex>
      </ModalContent>
    </StyledModal>
  );
};

export default Confirmation;
