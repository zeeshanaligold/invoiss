export const initialState = {
  items: [],
  item: null,
  error: null,
  loader: false,
  updatedItem: [],
  thead: ['#', 'ITEM', 'UOM', 'COST', 'QTY'],
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_UPDATES':
      return { ...state, ...action.payload };
    default:
      throw new Error('Unexpected action');
  }
};
