import React, { useReducer, useEffect } from 'react';
import RNPicker from 'rn-modal-picker';
import { withTheme } from '../../Context';
import styled from 'styled-components/native';
import API, { API_KEY } from '../../helpers/api';
import { reducer, initialState } from './reducer';
import { Container, Input, Button, Icon, Error } from '../../components';
import { Table, Cell, TableWrapper } from 'react-native-table-component';
import { ActivityIndicator, StyleSheet, Alert, ScrollView } from 'react-native';

const View = styled.View``;
const Hamburger = styled.TouchableOpacity`
  margin-left: 28px;
`;
const BtnText = styled.Text`
  color: #fff;
  margin-right: 10px;
`;
const BtnGroup = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: ${({ prev }) => (prev >= 0 ? ' space-between' : 'flex-end')};
`;
const UpdateInventory = ({ user, navigation }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const updateInventoryEntry = async () => {
    const { item } = state;
    const { scanner_id, scanner_code } = user.scanner;

    if (item.qty && item.id) {
      try {
        const { data } = await API.get('/UpdateInventoryEntry', {
          params: {
            api_key: API_KEY,
            scanner_id,
            scanner_code,
            sku: item.id,
            quantity: item.qty,
          },
        });
        if (data.inventory_entry) {
          let updatedItem = state.updatedItem.filter(el => el.id !== item.id);
          dispatch({
            type: 'SET_UPDATES',
            payload: {
              prev: updatedItem.length,
              updatedItem: [item, ...updatedItem],
              item: null,
            },
          });
        } else {
          dispatch({ type: 'SET_UPDATES', payload: { error: data.error_message } });
        }
      } catch (error) {
        dispatch({ type: 'SET_UPDATES', payload: { error: 'Something wrong...' } });
      }
    } else {
      Alert.alert('', 'Quantity & Sku is required!');
    }
  };

  const endInventoryRun = async () => {
    const { scanner_id, scanner_code } = user.scanner;
    dispatch({ type: 'SET_UPDATES', payload: { loader: true } });
    try {
      const { data } = await API.get('/EndInventoryRun', {
        params: {
          api_key: API_KEY,
          scanner_id,
          scanner_code,
        },
      });

      if (data.inventory_run) {
        dispatch({ type: 'SET_UPDATES', payload: { error: null, item: null, loader: false } });
        navigation.navigate('StartInventory');
      } else {
        dispatch({ type: 'SET_UPDATES', payload: { error: data.error_message, loader: false } });
      }
    } catch ({ response }) {
      dispatch({ type: 'SET_UPDATES', payload: { error: 'Something wrong...', loader: false } });
    }
  };

  const getInventoryItems = async () => {
    const { scanner_id, scanner_code } = user.scanner;
    try {
      const { data } = await API.get('/GetInventoryItems', {
        params: {
          api_key: API_KEY,
          scanner_id,
          scanner_code,
        },
      });
      if (data.items) {
        let items = [];
        for (let i = 0; i < data.items.length; i++) {
          for (let j = 0; j < data.items[i].item_uoms.length; j++) {
            items.push({
              item_name: data.items[i].item_name,
              id: data.items[i].item_uoms[j].item_uom_sku,
              uom_price: data.items[i].item_uoms[j].item_uom_price,
              uom_name: data.items[i].item_uoms[j].item_uom_abbreviation,
              name: data.items[i].item_name + ' (' + data.items[i].item_uoms[j].item_uom_sku + ' )',
            });
          }
        }
        dispatch({ type: 'SET_UPDATES', payload: { error: null, items } });
      } else {
        dispatch({ type: 'SET_UPDATES', payload: { error: data.error_message } });
      }
    } catch ({ response }) {
      dispatch({ type: 'SET_UPDATES', payload: { error: 'Something wrong...' } });
    }
  };

  useEffect(() => {
    getInventoryItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ScrollView>
      <Container>
        <Button colors={['#3F3F3F', '#3F3F3F']} onPress={endInventoryRun}>
          <BtnText>END INVENTORY</BtnText>
          <ActivityIndicator size="small" color="#fff" animating={state.loader} />
        </Button>
        <RNPicker
          showSearchBar={true}
          defaultValue={false}
          disablePicker={false}
          showPickerTitle={false}
          dataSource={state.items}
          changeAnimation={'none'}
          dummyDataSource={state.items}
          placeHolderLabel="SCAN BARCODE"
          pickerStyle={Styles.pickerStyle}
          searchBarPlaceHolder="SCAN BARCODE"
          selectedLabel={state.item && state.item.name}
          selectedValue={(index, item) =>
            dispatch({ type: 'SET_UPDATES', payload: { item: { ...item, qty: '1' } } })
          }
        />

        <View>
          <Input
            editable={false}
            autoCorrect={false}
            returnKeyType="next"
            placeholder="Item name"
            value={state.item && state.item.item_name}
            icon={<Icon name="shoppingBasketAdd" />}
          />
          <Input
            editable={false}
            placeholder="UOM"
            autoCorrect={false}
            returnKeyType="next"
            value={state.item && state.item.uom_name}
            icon={<Icon name="weightBalance" />}
          />
          <Input
            editable={false}
            autoCorrect={false}
            placeholder="COST"
            returnKeyType="next"
            keyboardType="decimal-pad"
            value={state.item && state.item.uom_price}
            icon={<Icon name="dollarOutlined" />}
          />
          <Input
            placeholder="QTY"
            autoCorrect={false}
            returnKeyType="next"
            value={state.item && state.item.qty}
            keyboardType="number-pad"
            icon={<Icon name="qty" />}
            onChangeText={v =>
              dispatch({ type: 'SET_UPDATES', payload: { item: { ...state.item, qty: v } } })
            }
          />

          <BtnGroup prev={state.prev}>
            {state.prev >= 0 && (
              <Button
                width="100px"
                onPress={() =>
                  dispatch({
                    type: 'SET_UPDATES',
                    payload: {
                      prev: state.prev - 1,
                      item: state.updatedItem[state.prev],
                    },
                  })
                }
              >
                <Icon color="#fff" name="arrowBackward" />
                <BtnText> BACK</BtnText>
              </Button>
            )}
            {state.item && (
              <Button width="100px" onPress={updateInventoryEntry}>
                <BtnText> SAVE</BtnText>
                <Icon color="#fff" name="arrow_forward" />
              </Button>
            )}
          </BtnGroup>
        </View>

        {state.error && <Error>{state.error}</Error>}
        <View>
          <Table>
            <TableWrapper style={Styles.header}>
              <Cell data="SAVED ITEMS" textStyle={Styles.headerText} />
            </TableWrapper>
            {state.updatedItem.map((item, index) => (
              <TableWrapper
                key={index}
                style={[Styles.dataRow, index % 2 && { backgroundColor: '#ffffff' }]}
              >
                <TableWrapper style={Styles.row}>
                  <Cell key={index} data={item.item_name} textStyle={Styles.itemName} />
                </TableWrapper>
                <TableWrapper style={Styles.row}>
                  <Cell key={index} data={item.id} textStyle={Styles.text} />
                  <Cell key={index} data={item.uom_name} textStyle={Styles.text} />
                  <Cell key={index} data={`$${item.uom_price}`} textStyle={Styles.text} />
                  <Cell key={index} data={item.qty} textStyle={Styles.text} />
                </TableWrapper>
              </TableWrapper>
            ))}
          </Table>
        </View>
      </Container>
    </ScrollView>
  );
};

const Styles = StyleSheet.create({
  pickerStyle: {
    height: 48,
    width: '96%',
    borderWidth: 2,
    marginLeft: 10,
    paddingRight: 30,
    marginBottom: 25,
    borderRadius: 10,
    flexDirection: 'row',
    borderColor: '#bdbdbd',
  },
  row: { flexDirection: 'row' },
  wrapper: { flexDirection: 'row' },
  headerText: { paddingLeft: 15, color: '#fff' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataRow: { padding: 15, backgroundColor: '#eeeeee' },
  header: { flexDirection: 'row', height: 40, backgroundColor: '#3F3F3F' },
  itemName: { textAlign: 'left', marginBottom: 5, textTransform: 'capitalize' },
});

UpdateInventory.navigationOptions = ({ navigation }) => ({
  headerLeft: (
    <Hamburger onPress={() => navigation.openDrawer()}>
      <Icon name="sidelist" color="#000" />
    </Hamburger>
  ),
});

export default withTheme(UpdateInventory);
