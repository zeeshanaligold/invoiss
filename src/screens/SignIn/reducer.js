export const initialState = {
  code: null,
  error: null,
  loader: false,
  password: null,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_UPDATES':
      return { ...state, ...action.payload };
    default:
      throw new Error('Unexpected action');
  }
};
