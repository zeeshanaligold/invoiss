import React, { useReducer } from 'react';
import { withTheme } from '../../Context';
import styled from 'styled-components/native';
import { ActivityIndicator } from 'react-native';
import { reducer, initialState } from './reducer';
import API, { API_KEY } from '../../helpers/api';
import { Container, Input, Button, Icon, Error } from '../../components';

const Image = styled.Image`
  width: 80%;
  height: 120px;
  margin: 20px auto 50px;
`;
const BtnText = styled.Text`
  color: #fff;
  margin-right: 10px;
`;
const SignIn = ({ navigation, handleUser }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleClick = async () => {
    const { code, password } = state;
    dispatch({ type: 'SET_UPDATES', payload: { loader: true } });
    try {
      const { data } = await API.get('/ScannerLogin', {
        params: {
          api_key: API_KEY,
          scanner_code: code,
          scanner_password: password,
        },
      });
      if (data.result === 'SUCCESS') {
        handleUser(data);
        navigation.navigate('StartInventory');
        dispatch({
          type: 'SET_UPDATES',
          payload: { error: data.error_message, code: '', password: '', loader: false },
        });
      } else {
        dispatch({ type: 'SET_UPDATES', payload: { error: data.error_message, loader: false } });
      }
    } catch ({ response }) {
      dispatch({ type: 'SET_UPDATES', payload: { error: 'Something wrong...', loader: false } });
    }
  };

  return (
    <Container justify="center">
      <Image resizeMode="contain" source={require('../../assets/images/invoiss.png')} />
      <Input
        value={state.code}
        autoCorrect={false}
        returnKeyType="next"
        placeholder="Scanner Code"
        icon={<Icon name="emailOutlined" />}
        onChangeText={v => dispatch({ type: 'SET_UPDATES', payload: { code: v } })}
      />
      <Input
        autoCorrect={false}
        returnKeyType="next"
        value={state.password}
        secureTextEntry={true}
        placeholder="Password"
        icon={<Icon name="lockOutlined" />}
        onChangeText={v => dispatch({ type: 'SET_UPDATES', payload: { password: v } })}
      />
      <Button label="" onPress={handleClick}>
        <BtnText>Sign In</BtnText>
        <ActivityIndicator size="small" color="#fff" animating={state.loader} />
      </Button>
      {state.error && <Error>{state.error}</Error>}
    </Container>
  );
};

SignIn.navigationOptions = {
  header: null,
};

export default withTheme(SignIn);
