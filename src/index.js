import { StatusBar } from 'react-native';
import styled from 'styled-components/native';
import React, { useState, useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from './Context';
import Routes from './Routes';

const Wrapper = styled.View`
  flex: 1;
`;
const App = () => {
  const [user, handleUser] = useState({});

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Provider value={{ user, handleUser }}>
      <Wrapper>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Routes />
      </Wrapper>
    </Provider>
  );
};

export default App;
