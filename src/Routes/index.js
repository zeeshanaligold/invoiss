import React from 'react';
import Drawer from './Drawer';
import Icon from '../components/Icon';
import styled from 'styled-components/native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { SignIn, UpdateInventory, StartInventory } from '../screens';

const BackButton = styled.TouchableOpacity`
  margin-left: 28px;
  border-radius: 50px;
`;
const SignInStack = createStackNavigator(
  {
    UpdateInventory: { screen: UpdateInventory },
    StartInventory: { screen: StartInventory },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        headerBackTitle: null,
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        flex: 1,
        fontSize: 14,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
      },
      headerLeft: (
        <BackButton onPress={() => navigation.goBack(null)}>
          <Icon name="arrowBackward" color="#000" />
        </BackButton>
      ),
    }),
    initialRouteName: 'StartInventory',
  }
);

const SignOutStack = createStackNavigator(
  {
    SignIn: { screen: SignIn },
  },
  {
    initialRouteName: 'SignIn',
    navigationOptions: {
      drawerLockMode: 'locked-closed',
    },
  }
);

const MainNavigator = createDrawerNavigator(
  {
    SignInStack: {
      screen: SignInStack,
    },
    SignOutStack: {
      screen: SignOutStack,
    },
  },
  {
    contentComponent: Drawer,
    drawerBackgroundColor: '#fff',
    initialRouteName: 'SignOutStack',
  }
);

const App = createAppContainer(MainNavigator);

export default App;
