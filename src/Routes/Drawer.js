import React from 'react';
import { withTheme } from '../Context';
import styled from 'styled-components';
import { ScrollView } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';

const Container = styled(LinearGradient)`
  flex: 1;
  height: 200px;
  align-items: center;
  justify-content: center;
`;
const Avatar = styled.View`
  width: 100px;
  height: 100px;
  border-width: 2;
  background: #fff;
  border-color: #ddd;
  align-items: center;
  border-radius: 100px;
  justify-content: center;
`;
const Title = styled.Text`
  color: #fff;
  font-size: 18px;
  margin-top: 10px;
`;
const AvatarTitle = styled.Text`
  color: #b08cf9;
  font-size: 30px;
`;
const Header = ({ customer }) => (
  <Container colors={['#07a7e3', '#b08cf9']} start={{ x: 0, y: 0 }} end={{ x: 0.7, y: 0 }}>
    <Avatar>
      <AvatarTitle>{customer && customer.customer_name.charAt(0)}</AvatarTitle>
    </Avatar>
    <Title>{customer && customer.customer_name}</Title>
  </Container>
);

const Menu = styled.View`
  flex: 1;
`;
const MenuItem = styled.Text`
  padding: 20px;
  color: #b08cf9;
  font-size: 17px;
  border-color: #b08cf9;
  border-bottom-width: 1;
`;
const Drawer = ({ navigation, user }) => {
  return (
    <ScrollView>
      <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
        <Header {...user} />
        <Menu>
          <MenuItem onPress={() => navigation.navigate('SignIn')}>SignOut</MenuItem>
        </Menu>
      </SafeAreaView>
    </ScrollView>
  );
};

export default withTheme(Drawer);
