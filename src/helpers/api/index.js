import axios from 'axios';
const API_URL = 'https://www.invoiss.com/api';
// api key
export const API_KEY = '9320E9A5DC7528580489CF038D088D33';

// defualt headers config
const defaultOptions = {
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
};

// create a new axios instance
const instance = axios.create(defaultOptions);

// set request
instance.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export default instance;
