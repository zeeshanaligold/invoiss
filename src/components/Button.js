import React from 'react';
import styled from 'styled-components/native';
import { TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Container = styled(LinearGradient)`
  height: 48px;
  display: flex;
  border-radius: 5px;
  flex-direction: row;
  align-items: center;
  margin-bottom: 25px;
  justify-content: center;
  width: ${({ width }) => width && width};
`;
const Text = styled.Text`
  color: #fff;
  text-align: center;
  font-family: 'Sofia-Pro-Regular';
  font-size: ${({ fontSize }) => fontSize && fontSize};
`;
const Button = ({ children, label, onPress, ...rest }) => (
  <TouchableOpacity onPress={onPress}>
    <Container {...rest}>{children ? children : <Text>{label}</Text>}</Container>
  </TouchableOpacity>
);

Text.defaultProps = {
  fontSize: '13px',
};

Container.defaultProps = {
  width: '100%',
  start: { x: 0, y: 0 },
  end: { x: 0.7, y: 0 },
  colors: ['#07a7e3', '#b08cf9'],
};

export default Button;
