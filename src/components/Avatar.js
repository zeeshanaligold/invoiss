import React from 'react';
import Icon from './Icon';
import styled from 'styled-components/native';

const Container = styled.View`
  width: 116px;
  height: 116px;
  border-radius: 116px;
  margin: 50px auto 56px;
  background-color: #d0d0d1;
`;
const Button = styled.TouchableOpacity`
  width: 48px;
  height: 48px;
  display: flex;
  margin-top: 68px;
  border-radius: 48px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: #30d158;
`;
const Avatar = () => {
  return (
    <Container>
      <Button>
        <Icon name="add" color="#fff" />
      </Button>
    </Container>
  );
};

export default Avatar;
