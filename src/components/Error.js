import styled from 'styled-components/native';

const Error = styled.Text`
  color: #fff;
  border-width: 1;
  padding: 5px 10px;
  border-color: red;
  border-radius: 5px;
  background-color: red;
`;

export default Error;
