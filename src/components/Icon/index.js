import React from 'react';
import icons from './icons';
import PropTypes from 'prop-types';
import Svg, { Path } from 'react-native-svg';

const Icon = ({ name, color, size }) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" strokeWidth="0.1" fill="none">
      <Path d={icons[name]} fill={color} stroke={color} />
    </Svg>
  );
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
};

Icon.defaultProps = {
  size: 24,
  color: '#D0D0D1',
};

export default Icon;
