import styled from 'styled-components/native';

const Container = styled.View`
  flex: 1;
  padding: ${({ Padding }) => Padding};
  justify-content: ${({ justify }) => justify && justify};
`;
Container.defaultProps = {
  Padding: '28px',
  justify: 'flex-start',
};

export default Container;
