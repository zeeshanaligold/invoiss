import React from 'react';
import styled from 'styled-components/native';

const Container = styled.View`
  padding-left: 10px;
  border-radius: 10px;
  flex-direction: row;
  align-items: center;
  border: 2px solid #bdbdbd;
  background-color: rgba(255, 255, 255, 0.15);
  margin-bottom: ${({ marginBottom }) => marginBottom && marginBottom};
`;
const TextInput = styled.TextInput.attrs({
  placeholderTextColor: '#d0d0d1',
})`
  width: 100%;
  height: 48px;
  color: #000000;
  padding: 10px 16px;
  border-radius: 10px;
  border: 2px solid #bdbdbd;
  font-family: 'Sofia-Pro-Regular';
  text-align: ${({ align }) => align && align};
  font-size: ${({ fontSize }) => fontSize && fontSize};
  margin-bottom: ${({ marginBottom }) => marginBottom && marginBottom};
  ${({ hasIcon }) => hasIcon && 'flex: 1; border: 0; margin-bottom: 0;'};
  background-color: ${({ hasIcon }) => (hasIcon ? 'transparent' : 'rgba(255, 255, 255, 0.15)')};
`;
const Input = ({ icon, ...rest }) => {
  return icon ? (
    <Container {...rest}>
      {icon}
      <TextInput hasIcon={icon} {...rest} />
    </Container>
  ) : (
    <TextInput {...rest} />
  );
};

Input.defaultProps = {
  align: 'left',
  fontSize: '14px',
  marginBottom: '25px',
};

export default Input;
